//
//  Certificat.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 7/11/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation

class Certificate {
    static let sharedInstance: Certificate = { Certificate()} ()
    var password        : String?
    var createAt        : Date?
    var expiredAt       : Date?
    var domainComponent : String?
    
    func saveData(){
        let defaults = UserDefaults.standard
        
        defaults.set(self.password, forKey: certificateKeys.password)
        defaults.set(self.createAt, forKey: certificateKeys.createAt.description)
        defaults.set(self.expiredAt, forKey: certificateKeys.expiredAt.description)
        defaults.set(self.domainComponent, forKey: certificateKeys.domainComponent)

    }
    
    func deleteData(){
        let defaults = UserDefaults.standard

        defaults.removeObject(forKey:certificateKeys.createAt)
        defaults.removeObject(forKey:certificateKeys.expiredAt)
        defaults.removeObject(forKey:certificateKeys.password)
        defaults.removeObject(forKey:certificateKeys.domainComponent)


    }
    
    func getData() -> Dictionary<String , Any>{
        var myData = Dictionary<String , Any>()
        let defaults = UserDefaults.standard
        
        if let password = defaults.string(forKey: certificateKeys.password) {
            myData[certificateKeys.password] = password
        }
        
        if let domainComponent = defaults.string(forKey: certificateKeys.domainComponent) {
            myData[certificateKeys.domainComponent] = domainComponent
        }
        
        if let createAt = defaults.object(forKey: certificateKeys.createAt) {
            myData[certificateKeys.createAt] = createAt
        }
        
        if let expiredAt = defaults.object(forKey: certificateKeys.expiredAt) {
            myData[certificateKeys.expiredAt] = expiredAt
        }
        
        return myData
    }
    
}

struct certificateKeys {
    static let password = "password"
    static let createAt = "createat"
    static let expiredAt = "expiredat"
    static let domainComponent = "domaincomponent"

}
