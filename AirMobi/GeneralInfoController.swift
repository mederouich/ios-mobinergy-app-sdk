//
//  GeneralInfoController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 6/8/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import UIKit
import AWSDK

class GeneralInfoController : UIViewController {
    
    @IBOutlet weak var usernameLabel    : UILabel!
    @IBOutlet weak var passwordLabel    : UILabel!
    @IBOutlet weak var acTypeLabel      : UILabel!
    @IBOutlet weak var isActiveLabel    : UILabel!
    @IBOutlet weak var userGroupLabel   : UILabel!
    @IBOutlet weak var userId           : UILabel!
    @IBOutlet weak var locationGroup    : UILabel!
    @IBOutlet weak var urlServer        : UILabel!
    @IBOutlet weak var consoleVersion   : UILabel!
    
    override func viewDidLoad() {
        

        
        // get information from session agent
        let awEnrol = AWController.clientInstance().account()
        
        usernameLabel   .text   = awEnrol?.username
        passwordLabel   .text   = awEnrol?.password
        urlServer       .text   = AWServer.sharedInstance().deviceServicesURL.deletingLastPathComponent().absoluteString
        consoleVersion  .text  = "\(AWServer.sharedInstance().consoleVersion().major).\(AWServer.sharedInstance().consoleVersion().minor).\(AWServer.sharedInstance().consoleVersion().release)"
        
        
        // send request to airwatch to get other additionnal information
        AWMDMInformationController.init().fetchUserInfo(completionBlock: { (success, userinfo, error) in
            // if requst is Successfull
            if(success){
                let mdmInfo = userinfo!  as NSDictionary // convert data to NSdictionary
                self.acTypeLabel.text    = mdmInfo.object(forKey: "AccountType")    as? String
                self.isActiveLabel.text  = mdmInfo.object(forKey: "IsActive")       as? String
                self.userGroupLabel.text = mdmInfo.object(forKey: "UserCategory")   as? String
                self.userId.text         = mdmInfo.object(forKey: "UserId")         as? String
                self.locationGroup.text  = mdmInfo.object(forKey: "LocationGroup")  as? String
                
            }
            
            if((error) != nil){

            }
            
            

        })
   
        super.viewDidLoad()
    }
    
    

    // TODO : get colors from airwatch console ... not finished
    func setBranding(){
        let commandManager = AWCommandManager.shared()
        let profile = commandManager?.sdkProfile()
        
        if(profile == nil ) {
            NSLog("there is no SDK Profile currently installed")
        } else {
            let  brandingPayload = profile?.brandingPayload
            if(brandingPayload != nil){
                let navigationBarColor = brandingPayload?.toolbarColor
                let primaryTextColor = brandingPayload?.primaryColor
                
                
            }
        }
    }
}
