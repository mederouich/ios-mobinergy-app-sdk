//
//  CertificateController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 7/6/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import UIKit
import AWSDK


class CertificateController : UIViewController    {


    @IBOutlet weak var certPassLabel: UILabel!
    @IBOutlet weak var certCreateAtLabel: UILabel!
    @IBOutlet weak var certExpiredAt: UILabel!
    @IBOutlet weak var certDcLabel: UILabel!
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        //TODO : not finished ...
        /**
        if(Certificate.sharedInstance.createAt != nil ){
            certPassLabel.text      = Certificate.sharedInstance.password
            certCreateAtLabel.text  = Certificate.sharedInstance.createAt?.description
            certExpiredAt.text      = Certificate.sharedInstance.expiredAt?.description
            certDcLabel.text        = Certificate.sharedInstance.domainComponent
        } else {
            var certificateData = Dictionary<String, Any>()
            certificateData = Certificate.sharedInstance.getData()
            certPassLabel.text      = certificateData[certificateKeys.password]         as? String
            certCreateAtLabel.text  = certificateData[certificateKeys.createAt]         as? String
            certExpiredAt.text      = certificateData[certificateKeys.expiredAt]        as? String
            certDcLabel.text        = certificateData[certificateKeys.domainComponent]  as? String
        }
         **/
        
    }
    

}
