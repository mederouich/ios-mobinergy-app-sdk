//
//  BrowserController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 6/8/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import UIKit
import AWSDK

class BrowserController: UIViewController , UIWebViewDelegate , URLSessionDelegate , URLSessionTaskDelegate  {

    @IBOutlet weak var urlvalue: UITextField!
    @IBOutlet weak var webViewBrowser: UIWebView!
    var urlValue = "https://ex01.emmlab.mobi:8443/" 

    
    override func viewDidLoad() {
        webViewBrowser.delegate = self
        urlvalue.text = urlValue
        if let url = URL (string: urlValue){
            //let requestObj = URLRequest(url: url)
            self.sessionGetRequest(url)
            LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "ViewDidLoad", lineNbr: 0, msg: "start navigation with \(url.absoluteString)")

        }


    }

    
    @IBAction func browseToUrl(_ sender: Any) {
        webViewBrowser.delegate = self
        if let url = URL (string: (getURLStringFromTextField())){
            self.sessionGetRequest(url)
            LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "browsetoUrl", lineNbr: 0, msg: "start navigation with \(url.absoluteString)")
        }
    }
    

    

    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // Printing a message here to demonstrate the callback being called.
        
        
        if challenge.previousFailureCount > 1 {
            LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "Challenge failure count :  \(challenge.previousFailureCount)")
            // Cancel the request/challenge if more than 1 attempt has failed, will display a message to the user
            completionHandler(.cancelAuthenticationChallenge, nil)
            
        } else {
            /*
             * For debugging purposes, we're printing the Authentication Method of the endpoint.
             * This was helpful in determining if the endpoint the api was hitting, was actually able
             * to use Integrated Authentication.
             */
            LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "challenge for switch case is : \(challenge.protectionSpace.authenticationMethod)")

            switch challenge.protectionSpace.authenticationMethod {
                /*
                 * Integrated Authentication does not handle Server Trust, if an endpoint is presenting this
                 * as the authentication method, then the developer needs to handle that prior to Integrated
                 * Authentication handling authentication. Below we're checking to see if the method is explicitly
                 * server trust. We're using an if/else statement to handle server trust if need and perform integrated
                 * authentication otherwise.
                 */
            case NSURLAuthenticationMethodServerTrust:
                /*
                 * The completion handler is handling server trust below. We're telling it to handle it and not
                 * passing it any credentials
                 */
                LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "challenge NSURLAuthenticationMethodServerTrust handler  ")
                completionHandler(.performDefaultHandling,nil)
                break
                /*
                 Below are the three types of authentication type that is supporedted by SDK.
                 Checking if one of the suppored authentication is received and
                 calling SDK's handle challenge method to handle the corresponding challenge
                 */
            case NSURLAuthenticationMethodHTTPBasic:
                LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "challenge NSURLAuthenticationMethodHTTPBasic handler  ")

                // authentication with login / password basic
                handleAirWatchIntegratedAuthenticationforSession(challenge,completionHandler: completionHandler)
                break
            case NSURLAuthenticationMethodNTLM:
                LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "challenge NSURLAuthenticationMethodNTLM handler  ")

                // authentication with windows credential
                handleAirWatchIntegratedAuthenticationforSession(challenge,completionHandler: completionHandler)
                break
            case NSURLAuthenticationMethodClientCertificate:
                LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "challenge NSURLAuthenticationMethodClientCertificate handler  ")

                // authentication with certificate
                handleAirWatchIntegratedAuthenticationforSession(challenge,completionHandler: completionHandler)

                break
                /*
                 If the auth challenge type is any other then basic, NTLM or cert auth
                 then it's not supported by SDK and developer has to handle it manually
                 */
            default :
                LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "no challenge found ")

                completionHandler(.cancelAuthenticationChallenge, nil)
            }
            
        }
        
    }
   
    
    //MARK: NSURLSession
    
    func sessionGetRequest(_ url: URL) {
        
        
        //Creating request
        let request = NSMutableURLRequest(url: url)
        
        //Creating session with configuration
        let configuration = Foundation.URLSession.shared.configuration
        let session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        
        //Handling the data and response returned by session task
        let task = session.dataTask(with: request as URLRequest)  {
            taskData, taskResponse, error in
            if let data = taskData, let response: HTTPURLResponse = taskResponse! as? HTTPURLResponse {
                

                if(response.mimeType != nil && response.textEncodingName != nil && response.url != nil)
                {
                    
                    // Updating the UI on the Main thread.
                    OperationQueue.main.addOperation({
                        // refresh browser view
                        self.webViewBrowser.load(data, mimeType: response.mimeType!, textEncodingName: response.textEncodingName!, baseURL: response.url!)
                    })
                }
                    
                else
                {
                    
                    let dataString = String(data: data, encoding:String.Encoding(rawValue: self.getCorrectEncoding(response)) )
                    // Updating the UI on the Main thread.
                    OperationQueue.main.addOperation({
                        self.webViewBrowser.loadHTMLString(dataString!, baseURL:response.url!)
                        
                    })
                }
                session.invalidateAndCancel()
                session.finishTasksAndInvalidate()
            }
        }
        task.resume()
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if(error != nil)
        {
            LogManager.shared.error(fileName: FileNameLogger.Browsing , methodName: "urlSession", lineNbr: 0, msg: "error urlSession \(error.debugDescription) ")

        }
        
    }
    
    /*
     * In order to leverage Integrated Authentication two steps need to be performed.
     * First a check that the protection space is one that the SDK can handle and second,
     * that it has the credentials needed in order to move forward. If both of these checks
     * are ok then call "handleChallenge" in order for the SDK to take care of the challenge.
     */
    func handleAirWatchIntegratedAuthenticationforSession(_ challenge: URLAuthenticationChallenge,completionHandler: @escaping (Foundation.URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        do {
            LogManager.shared.info(fileName: FileNameLogger.Browsing , methodName: "handleAirWatchIntegratedAuthenticationforSession", lineNbr: 0, msg: "start airwatch integrated authentication handler \(challenge.protectionSpace) ")

            try AWController.clientInstance().canHandle(challenge.protectionSpace)
            AWController.clientInstance().handleChallenge(forURLSessionChallenge: challenge, completionHandler: { (disposition, credential) in
                
                completionHandler(disposition, credential)
            })
        } catch {
            LogManager.shared.error(fileName: FileNameLogger.Browsing , methodName: "handleAirWatchIntegratedAuthenticationforSession", lineNbr: 0, msg: "can not handle airwatch integrated authentication for Session ")

        }
    }
    


    /*
     Different websites return different kind of encoding
     getting the correct encoding from the response which we used later
     to populated and render the data returned by webiste inside the webivew.
     */
    func getCorrectEncoding(_ response : URLResponse) -> UInt
    {
        var usedEncoding = String.Encoding.utf8
        if let encodingName = response.textEncodingName {
            let encoding = CFStringConvertEncodingToNSStringEncoding(CFStringConvertIANACharSetNameToEncoding(encodingName as CFString))
            if encoding != UInt(kCFStringEncodingInvalidId) {
                usedEncoding = String.Encoding(rawValue: encoding)
            }
            return usedEncoding.rawValue
        }
        else
        {
            return usedEncoding.rawValue
        }
    }
    
    //Check for the formatting of the entered URL
    func getURLStringFromTextField() -> String {
        var urlString = urlvalue.text
        
        
        if(urlString!.isEmpty)
        {
            urlString = "https://www.mobinergy.com"
            
        }
        else if (!(urlString!.hasPrefix("http://")) && !(urlString!.hasPrefix("https://")))
        {
            urlString! = "https://" + urlString!
        }
        
        urlString! = urlString!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        print("Final URL is \(urlString!)")
        return urlString!
    }
    
    
}
