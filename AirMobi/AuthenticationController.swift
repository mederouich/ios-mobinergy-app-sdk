//
//  AuthenticationController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 7/14/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import UIKit
import AWSDK


class AuthenticationController : UIViewController {

    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    
    override func viewDidLoad() {
        LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "ViewDidLoad", lineNbr: 0, msg: "open AuthenticationController")

        // get information from session
        let awEnrol = AWController.clientInstance().account()
        
        if(awEnrol != nil ){
            LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "ViewDidLoad", lineNbr: 0, msg: "AWController.clientInstance().account() is not null ")
        } else {
            LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "ViewDidLoad", lineNbr: 0, msg: "AWController.clientInstance().account() is null")

        }
        self.usernameLbl   .text   = awEnrol?.username
        self.passwordLbl   .text   = awEnrol?.password
        
        LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "ViewDidLoad", lineNbr: 0, msg: "get Authentication Information ")
        LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "ViewDidLoad", lineNbr: 0, msg: "username : \(awEnrol?.username) || password : \(awEnrol?.password) ")



        super.viewDidLoad()
    }
    
    
    /**
     * If SSO is enabled in the SDK profile, then the app will invoke the Agent to update the credentials.
     * If SSO is disabled, then the authentication update prompt will take place within the SDK application.
     **/
    @IBAction func updateCredentialAction(_ sender: Any) {
        LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "updateCredentialAction", lineNbr: 0, msg: "call action  updateCrentialAction ")

        AWController.clientInstance().updateUserCredentials { (success , error) in
            if(success){
                LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "updateCredentialAction", lineNbr: 0, msg: "get response success from updateCredentials method  ")
                self.usernameLbl.text = AWController.clientInstance().account().username
                self.passwordLbl.text = AWController.clientInstance().account().password
            }
            if ((error) != nil){
                LogManager.shared.error(fileName: FileNameLogger.authentication , methodName: "updateCredentialAction", lineNbr: 0, msg: "get error from updateUserCredentials \(error.debugDescription) ")

            }
        }
    }
}
