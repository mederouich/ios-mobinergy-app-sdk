//
//  ViewController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 5/31/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import UIKit
import AWSDK

class MenuController: UIViewController    {
    var loadingView  = LoadingIndicatorView()

    @IBOutlet weak var sendLogBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
       self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    

    @IBAction func sendLogAction(_ sender: Any) {
        LogManager.shared.sendLog()
        LoadingIndicatorView.show("Send Logs ...")


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func hideBlocker() {
        
        LoadingIndicatorView.hide()
       
        
    }
    
    

    
    


}

