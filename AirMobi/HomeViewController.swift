//
//  HomeViewController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 6/22/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import UIKit
import AWSDK


class HomeViewController : UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnActionConnectWithAgent(_ sender: Any) {
        
        
        let appDelegate               = UIApplication.shared.delegate as! AppDelegate
        let sdkStatus                 = appDelegate.awSDKInit
        let controller : AWController = AWController.clientInstance()
        controller.callbackScheme     = "airmobi" // our application schema
        controller.delegate           = appDelegate
        

        if  (sdkStatus == false)
        {
            LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "btnActionConnectWithAgent", lineNbr: 0, msg: "Initializing SDK")
            LoadingIndicatorView.show("Initializing SDK...")
            // get the session from airwatch agent
             AWController.clientInstance().start()

        }
        
    }
}
