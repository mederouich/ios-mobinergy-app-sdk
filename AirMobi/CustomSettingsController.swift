//
//  CustomSettingsController.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 6/22/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import UIKit
import AWSDK

class CustomSettingsController : UIViewController {
    
    @IBOutlet weak var deviceModel      : UILabel!
    @IBOutlet weak var devicePlatform   : UILabel!
    @IBOutlet weak var DeviceUid        : UILabel!
    @IBOutlet weak var deviceOS         : UILabel!
    @IBOutlet weak var deviceWlanMac    : UILabel!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  let certificatePayload = AWCommandManager().appProfile()
      //  NSLog("certificate Profile is : \(certificatePayload?.toDictionary())")
        
        let customPayload  = AWCommandManager().sdkProfile().customPayload
    
        //Checking if the Custom Settings Payload is nil or not set
        if customPayload != nil
        {
            
            let customSettings = customPayload?.settings
            if(customSettings == "")
            {
                NSLog("no customSettings" )
            }
            else
            {
                let data = convertToDictionary(text : customSettings!)
                if let deviceModel = data?["DeviceModel"] { // The `?` is here because our `convertStringToDictionary` function returns an Optional
                    self.deviceModel.text = deviceModel as! String
                }
                if let devicePlatform = data?["DevicePlatform"] { // The `?` is here because our `convertStringToDictionary` function returns an Optional
                    self.devicePlatform.text = devicePlatform as! String
                }
                if let deviceUid = data?["DeviceUid"] { // The `?` is here because our `convertStringToDictionary` function returns an Optional
                    self.DeviceUid.text = deviceUid as! String
                }
                if let deviceWlanMac = data?["DeviceWLANMac"] { // The `?` is here because our `convertStringToDictionary` function returns an Optional
                    self.deviceWlanMac.text = deviceWlanMac as! String
                }
                if let deviceOS = data?["DeviceOperatingSystem"] { // The `?` is here because our `convertStringToDictionary` function returns an Optional
                    self.deviceOS.text = deviceOS as! String
                }
                
            }
            
        }
        else
        {
            
            
        }

    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
