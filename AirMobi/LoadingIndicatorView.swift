//
//  iOS Swift Sample App
//
//  Created by Nitin Sharma on 10/15/16.
//  Copyright © 2016 VMware. All rights reserved.
//
import UIKit

class LoadingIndicatorView {
    
    static var BlockerScreen : UIView?
    
    static func show() {
        guard let BlockerWindow = UIApplication.shared.keyWindow else {
             return
        }
        show(BlockerWindow)
    }
    
    static func show(_ _loadingText: String) {
        guard let BlockerWindow = UIApplication.shared.keyWindow else {
             return
        }
        show(BlockerWindow, loadingText: _loadingText)
    }
    
    static func show(_ _callerViewTarget : UIView) {
        show(_callerViewTarget, loadingText: nil)
    }
    
    static func show(_ _callerViewTarget : UIView, loadingText: String?) {
        // Hiding the Blocker if it's already being shown
        hide()
        
        
        //Creaing the Blocker screen
        let localBlocker = UIView(frame: _callerViewTarget.frame)
        localBlocker.center = _callerViewTarget.center
        localBlocker.alpha = 0
        localBlocker.backgroundColor = UIColor.black
        _callerViewTarget.addSubview(localBlocker)
        _callerViewTarget.bringSubview(toFront: localBlocker)
        
        // Setting the Loading Spinner
        let loadingSpinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        loadingSpinner.center = localBlocker.center
        loadingSpinner.startAnimating()
        localBlocker.addSubview(loadingSpinner)
        
        // Setting the text for the loading spinner
        if let textString = loadingText {
            let loadingText = UILabel()
            loadingText.text = textString
            loadingText.textColor = UIColor.white
            loadingText.sizeToFit()
            loadingText.center = CGPoint(x: loadingSpinner.center.x, y: loadingSpinner.center.y + 30)
            localBlocker.addSubview(loadingText)
        }
        
        // Showing the blocker and animating
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        localBlocker.alpha = localBlocker.alpha > 0 ? 0 : 0.5
        UIView.commitAnimations()
        
        BlockerScreen = localBlocker
    }
    
    static func hide() {
        if BlockerScreen != nil {
            BlockerScreen?.removeFromSuperview()
            BlockerScreen =  nil
        }
    }
}
