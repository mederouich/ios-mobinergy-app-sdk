//
//  AppDelegate.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 5/31/17.
//  Copyright © 2017 Mobinergy || Mohammed Derouich. All rights reserved.
//

import UIKit
import AWSDK
import Security


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , AWSDKDelegate {

    var window: UIWindow?
    var awSDKInit: Bool? = false
    var sdkUseCase = MenuController()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //  let controller : AWController = AWController.clientInstance()
        //   controller.callbackScheme = "airmobi"
        //  controller.delegate = self
        
        // TODO :  get the certificate profile app
        // Define identifier
        //  let notificationName = Notification.Name(AWNotificationCommandManagerInstalledNewProfile)
        
        // Register to receive notification
        //  NotificationCenter.default.addObserver(self, selector: #selector(handleAppProfile), name: notificationName, object: nil)
        return true
        
    }



    // TODO : method observable if airwatch send credential profil to our app
    func handleAppProfile(notification: Notification) {
        let profileCert  : AWProfile = notification.object as! AWProfile
 
 
        let certificatePayload : AWCertificatePayload = profileCert.certificatePayload
        
                if(certificatePayload != nil ){
                let certData   = certificatePayload.certificateData!
                let certificatPassword = certificatePayload.certificatePassword
                
                Certificate.sharedInstance.password = certificatPassword!
                self.extractIdentity(certData: certData as NSData, certPassword: certificatPassword!)
                    


                }
    
        
        
    }




    public struct IdentityAndTrust {
        public var identityRef:SecIdentity
        public var trust:SecTrust
        public var certArray:NSArray
    }
    
    
    public func extractIdentity(certData:NSData, certPassword:String)  {
        
       // var identityAndTrust:IdentityAndTrust!
        var securityError:OSStatus = errSecSuccess
        
        var items: CFArray?
        let certOptions: Dictionary = [ kSecImportExportPassphrase as String : certPassword ];
        // import certificate to read its entries
        securityError = SecPKCS12Import(certData, certOptions as CFDictionary, &items);
        
        if securityError == errSecSuccess {
            
            
            let certItems:CFArray = items as CFArray!;
            let certItemsArray:Array = certItems as Array
            let dict:AnyObject? = certItemsArray.first;
            
            if let certEntry:Dictionary = dict as? Dictionary<String, AnyObject> {
                
                // grab the identity
                let identityPointer:AnyObject? = certEntry["identity"];
                let secIdentityRef:SecIdentity = identityPointer as! SecIdentity!;
                
                // grab the trust
             //   let trustPointer:AnyObject? = certEntry["trust"];
             //   let trustRef:SecTrust = trustPointer as! SecTrust;
                
                
                // grab the certificate chain
                var certRef: SecCertificate?
                SecIdentityCopyCertificate(secIdentityRef, &certRef)
                let certArray:NSMutableArray = NSMutableArray();
                certArray.add(certRef as SecCertificate!);
                
            //    let certInfo = certRef.debugDescription
                
                
                
                let certificatewee = SecCertificateCopyData(certRef!)
                // DC = mobi CN = emmlab
                let dc = X509Wrapper.getIssuerName(certificatewee as Data!)
                let dateExpired = X509Wrapper.getExpiryDate(certificatewee as Data!)
                let dateStart = X509Wrapper.getStartDate(certificatewee as Data!)
                
                Certificate.sharedInstance.domainComponent = dc!
                Certificate.sharedInstance.expiredAt = dateExpired!
                Certificate.sharedInstance.createAt = dateStart!
                Certificate.sharedInstance.saveData()
                
            }
        }

        
    }
    
    
    
    // call airwatch agent with correct schema : airwatch or awssobroker2
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
            LogManager.shared.info(fileName: FileNameLogger.authentication , methodName: "application", lineNbr: 0, msg: "Appdelegate hamdle Open Url with  source Application : \(sourceApplication)")
            return AWController.clientInstance().handleOpen(url, fromApplication: sourceApplication)
       
        
    }
    

    // if get data from airwatch agent with somes errors
    func initialCheckDoneWithError(_ error: Error!) {
        NSLog("initialCheckDoneWithError called")

        
        if error != nil {
            
            NSLog("initialCheckDone With  Error")
            awSDKInit=true
            let alertController = UIAlertController(title: "AWInit  Reporter", message:
                "An error occured while initializing AW SDK : ", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            sdkUseCase.hideBlocker()

            
        }
        else {
            
            sdkUseCase.hideBlocker()
            NSLog("initialCheckDone NO Error")
            awSDKInit=true
 
            
        }
 
    }
    

    
    // Receive app and sdk profile from airwatch
    func receivedProfiles(_ profiles: [Any]!) {
        
        
        if profiles != nil {
            
            if let awProfiles = profiles as? [AWProfile] {

                for profile in awProfiles {
                    //AWCommandManager.shared().loadCommands()

                    switch(profile.profileType){
                    case AWAppProfile :
                        let profilo = profile.toDictionary()
                        break
                    case AWSDKProfile :
                        let profilo = profile.toDictionary()

                        break

                    default :
                        NSLog("Default profile" )
                        break
                    }

                }
            }

        }
        else {
            NSLog("receivedProfiles is nil")
        }
        
    }
    
    
    func wipe() {
        
        NSLog("wipe")
    }
    
    func lock() {
        NSLog("lock")
        
        
    }
    
    func unlock() {
        NSLog("unlock")
    }
    
    func resumeNetworkActivity() {
        NSLog("resumeNetworkActivity")
    }

    
    
    func stopNetworkActivity(_ networkActivityStatus: AWNetworkActivityStatus) {
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {

     //Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     //return AWController.clientInstance().start()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}



