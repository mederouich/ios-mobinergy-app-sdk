//
//  LogManager.swift
//  AirMobi
//
//  Created by Mohammed Derouich on 6/22/17.
//  Copyright © 2017 Mohammed Derouich. All rights reserved.
//

import Foundation
import AWSDK

class LogManager {
    

    static let shared = LogManager()

    // MARK: - Initialization Method
    
    private init() { }
    
    
    func info(fileName : String , methodName : String , lineNbr : Int , msg : String){
        AWLog.sharedInstance().log(withLogLevel: AWLogLevelInfo, file: fileName, methodName: methodName, line: lineNbr, message: msg)
    }
    
    func verbose(fileName : String , methodName : String , lineNbr : Int , msg : String){
        AWLog.sharedInstance().log(withLogLevel: AWLogLevelVerbose, file: fileName, methodName: methodName, line: lineNbr, message: msg)
    }
    
    func warning(fileName : String , methodName : String , lineNbr : Int , msg : String){
        AWLog.sharedInstance().log(withLogLevel: AWLogLevelWarning, file: fileName, methodName: methodName, line: lineNbr, message: msg)
    }
    
    func error(fileName : String , methodName : String , lineNbr : Int , msg : String){
        AWLog.sharedInstance().log(withLogLevel: AWLogLevelError, file: fileName, methodName: methodName, line: lineNbr, message: msg)
    }
    
    
    func sendLog(){
        AWLog.sharedInstance().sendApplicationLogs { (success, error) in
            if(false == success){
                NSLog("error: \(String(describing: error?.localizedDescription))");
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    LoadingIndicatorView.hide()
                }
            } else {
                NSLog("Sucess")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    LoadingIndicatorView.hide()
                }
            }
        }
    }
}

struct FileNameLogger {
    static let authentication       = "Authentication.log"
    static let CredentialProfile    = "CredentialProfile"
    static let Browsing             = "Browsing"

    
}
